﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client
{
    class ParkingManager
    {
        private RequestSender _sender;

        public ParkingManager()
        {
            _sender = new RequestSender();
        }

        public decimal RecentMoney()
        {
            decimal money = 0;
            var transactions = _sender.GetTransactions();
            foreach(var transaction in transactions)
            {
                money += transaction.TransactionCount;
            }
            return money;
        }

        public List<Transaction> GetTransactions()
        {
            return _sender.GetTransactions();
        }

        public List<Car> GetCars()
        {
            return _sender.GetCars();
        }

        public Car GetCar(int id)
        {
            return _sender.GetCar(id);
        }

        public void AddTransport(Car car)
        {
            _sender.PostCar(car);
        }

        public void RemoveTransport(int id)
        {
            _sender.DeleteCar(id);
        }

        public void ChangeTransport(int id, Car car)
        {
            _sender.PutCar(id, car);
        }

        public int GetCountOfCars()
        {
            var cars = _sender.GetCars();
            return cars.Count;
        }

        public decimal GetCurrentBalance()
        {
            return _sender.GetBalance();
        }
    }
}
