﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public enum CarType
    {
        Light,
        Cargo,
        Bus,
        Motocycle
    }

    public class Car
    {
        public string CarName { get; set; }
        public CarType Type { get; set; }
        public decimal Balance { get; set; }

        public Car(string carName, CarType type, decimal balance)
        {
            CarName = carName;
            Type = type;
            Balance = balance;
        }
    }
}
