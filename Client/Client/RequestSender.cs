﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Client
{
    class RequestSender
    {
        private HttpClient _client;

        //Initilize HttpClient
        public RequestSender()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri("https://localhost:44355");
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        //Get some Data From WebApi By url
        public string GetData(string path)
        {
            HttpResponseMessage response = _client.GetAsync(path).Result;
            return response.Content.ReadAsStringAsync().Result;
        }

        public List<Car> GetCars()
        {
            string responseBody = GetData("api/parking");
            var values = JsonConvert.DeserializeObject<Car[]>(responseBody);
            
            return new List<Car>(values);
        }

        public Car GetCar(int id)
        {
            string responseBody = GetData($"api/parking/{id}");
            var car = JsonConvert.DeserializeObject<Car>(responseBody);
            return car;
        }

        public void PostCar(Car car)
        {
            var result = _client.PostAsJsonAsync("api/parking", car).Result;
        }

        public void PutCar(int id, Car car)
        {
            var result = _client.PutAsJsonAsync($"api/parking/{id}", car).Result;
        }

        public void DeleteCar(int id)
        {
            var result = _client.DeleteAsync($"api/parking/{id}").Result;
        }

        public List<Transaction> GetTransactions()
        {
            string responseBody = GetData("api/transactions");
            var transactions = JsonConvert.DeserializeObject<Transaction[]>(responseBody);

            return new List<Transaction>(transactions);
        }

        public decimal GetBalance()
        {
            string responseBody = GetData("api/balance");
            return JsonConvert.DeserializeObject<decimal>(responseBody);
        }
    }
}
