﻿using System;
using System.Collections.Generic;

namespace Client
{
    class Program
    {
        private static int MenuOutput()
        {
            Console.WriteLine("<============> Menu <=================>");
            Console.WriteLine("1 - See Curent Balance Of Parking");
            Console.WriteLine("2 - Money Earned For Last Minute");
            Console.WriteLine("3 - Count Of Free/Busy Places");
            Console.WriteLine("4 - Print Transactions Of Last Minute");
            Console.WriteLine("5 - Print All Transactions");
            Console.WriteLine("6 - Print All Transport");
            Console.WriteLine("7 - Put Transport In Parking");
            Console.WriteLine("8 - Take Transport From Parking");
            Console.WriteLine("9 - Refill Car Balance");
            Console.WriteLine("10 - Close");
            string answer = Console.ReadLine();
            return Convert.ToInt32(answer);
        }

        private static void ClearConsole()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            Console.Clear();
        }

        private static Car CreateCar()
        {
            Console.WriteLine("Input: Car Name | Car Type (Light, Cargo, Bus, Motocycle) | Car Balance");
            var lines = Console.ReadLine().Split();
            if (lines.Length != 3) return null;
            CarType type;
            switch (lines[1].ToLower())
            {
                case "light":
                    type = CarType.Light;
                    break;
                case "bus":
                    type = CarType.Bus;
                    break;
                case "motocycle":
                    type = CarType.Motocycle;
                    break;
                case "cargo":
                    type = CarType.Cargo;
                    break;
                default:
                    return null;
            }
            decimal balance;
            try
            {
                balance = Convert.ToDecimal(lines[2]);
            }
            catch (Exception e)
            {
                return null;
            }

            return new Car(lines[0], type, balance);
        }

        public static void PrintCars(ParkingManager manager)
        {
            var cars = manager.GetCars();
            for (int i = 0; i < cars.Count; i++)
            {
                Console.WriteLine($"Id: {i} | Car Name: {cars[i].CarName} | Car Type: {cars[i].Type} | Car Balance: {cars[i].Balance}");
            }
        }

        public static void PrintTransactions(List<Transaction> transactions)
        {
            foreach(var transaction in transactions)
            {
                Console.WriteLine($"Time: {transaction.Time} | Car Type: {transaction.Type} | Count: {transaction.TransactionCount}");
            }
        }

        public static int ChooseCar(ParkingManager manager)
        {
            PrintCars(manager);
            int id;
            Console.WriteLine("Choose id: ");
            try
            {
                id = Convert.ToInt32(Console.ReadLine());
                if(id < 0 || id >= manager.GetCountOfCars())
                {
                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Wrong Input");
                return -1;
            }

            return id;
        }

        public static void RunMenu(ParkingManager manager)
        {
            bool stop = false;
            while (!stop)
            {
                var answer = MenuOutput();
                switch (answer)
                {
                    case 1:
                        Console.WriteLine(manager.GetCurrentBalance());
                        ClearConsole();
                        break;
                    case 2:
                        Console.WriteLine(manager.RecentMoney());
                        ClearConsole();
                        break;
                    case 3:
                        Console.WriteLine("Unavailable.In Process...");
                        ClearConsole();
                        break;
                    case 4:
                        var transactions = manager.GetTransactions();
                        PrintTransactions(transactions);
                        ClearConsole();
                        break;
                    case 5:
                        Console.WriteLine("Unavailable.In Process...");
                        ClearConsole();
                        break;
                    case 6:
                        PrintCars(manager);
                        ClearConsole();
                        break;
                    case 7:
                        var car = CreateCar();
                        if (car != null) manager.AddTransport(car);
                        ClearConsole();
                        break;
                    case 8:
                        int id = ChooseCar(manager);
                        if (id != -1) manager.RemoveTransport(id);
                        ClearConsole();
                        break;
                    case 9:
                        id = ChooseCar(manager);
                        if (id != -1)
                        {
                            Console.WriteLine("Input Sum: ");
                            try
                            {
                                decimal sum = Convert.ToDecimal(Console.ReadLine());
                                car = manager.GetCar(id);
                                car.Balance = sum;
                                manager.ChangeTransport(id, car);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Wrong input");
                            }
                        }
                        ClearConsole();
                        break;
                    case 10:
                        stop = true;
                        break;
                }
            }
        }



        static void Main(string[] args)
        {
            ParkingManager manager = new ParkingManager();
            RunMenu(manager);
            

        }
    }
}
