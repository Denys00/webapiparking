﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using parking;
using Server.Services;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : Controller
    {
        private ParkingService _service;

        public ParkingController(ParkingService service)
        {
            _service = service;
        }
        // GET api/values
        [HttpGet]
        public IEnumerable<Car> Get()
        {
            return _service.GetCars();
        }
        // GET api/values/5
        [HttpGet("{id}")]
        public Car Get(int id)
        {
            return _service.GetCar(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] Car value)
        {
            _service.AddCar(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Car value)
        {
            _service.ChangeCar(id, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _service.DeleteCar(id);   
        }
    }
}
