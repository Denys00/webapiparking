﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Server.Services;
using parking;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private TransactionsService _service;

        public TransactionsController(TransactionsService service)
        {
            _service = service;
        }

        // GET: api/Transactions
        [HttpGet]
        public IEnumerable<Transaction> Get()
        {
            return _service.GetTransactions();
        }

        // GET: api/Transactions/5
        [HttpGet("{id}", Name = "Get")]
        public Transaction Get(int id)
        {
            return _service.GetTransaction(id);
        }
    }
}
