﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Server.Services;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BalanceController : ControllerBase
    {
        private TransactionsService _service;

        public BalanceController(TransactionsService service)
        {
            _service = service;
        }

        // GET: api/Balance
        [HttpGet]
        public decimal Get()
        {
            return _service.GetBalance();
        }

    }
}
