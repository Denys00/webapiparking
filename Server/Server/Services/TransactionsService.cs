﻿using parking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Server.Services
{
    public class TransactionsService
    { 
        private HttpClient _client;
        private Parking _park;

        public TransactionsService()
        {
            _client = new HttpClient();
            _park = Parking.Initilize();
        }

        public IEnumerable<Transaction> GetTransactions()
        {
            return _park.GetTransactions();
        }

        public Transaction GetTransaction(int id)
        {
            return _park.GetTransactions()[id];
        }

        public decimal GetBalance()
        {
            return _park.Balance;
        }
    }
}
