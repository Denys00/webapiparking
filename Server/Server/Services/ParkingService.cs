﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using parking;

namespace Server.Services
{
    public class ParkingService
    {
        private HttpClient _client;
        private Parking _park;

        public ParkingService()
        {
            _client = new HttpClient();
            _park = Parking.Initilize();
            _park.LogPath = @"C:\MyProjects\binary-studio\webapiparking\Server\Server\Parking\Transactions.log";
            RunParking();
        }

        public Task RunParking()
        {
            return _park.RunTelepromter();
        }

        public IEnumerable<Car> GetCars()
        {
            return _park.GetCars();
        }

        public Car GetCar(int id)
        {
            return _park.GetCars()[id];
        }

        public void AddCar(Car car)
        {
            _park.AddCar(car);
        }

        public void DeleteCar(int id)
        {
            _park.DeleteCar(id);
        }

        public void ChangeCar(int id, Car car)
        {
            _park.ReplaceCarAt(id, car);
        }

    }
}
