﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parking
{
    static class ParkingConf
    {
        public static decimal StartBalance { get; set; } = 0;
        public static int MaxCount { get; set; } = 10;
        public static int TransactionPeriod { get; set; } = 5;
        public static Dictionary<CarType, decimal> CarsPrice = new Dictionary<CarType, decimal>
        {
            [CarType.Bus] = 3.5M,
            [CarType.Cargo] = 5,
            [CarType.Motocycle] = 1,
            [CarType.Light] = 2

        };
        public static decimal FairCoeff { get; set; } = 2.5m;
        public static bool Done { get; private set; }
        public static int LogPeriod { get; set; } = 60;
        public static void SetDone()
        {
            Done = true;
        }
    }
}
