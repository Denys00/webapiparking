﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parking
{
    public class Parking
    {
        public decimal Balance { get; set; }
        private List<Car> cars;
        private List<Transaction> transactions;
        public string LogPath { get; set; }

        public void ReplaceCarAt(int id, Car car)
        {
            cars[id] = car;
        }

        public void AddCar(Car car)
        {
            if(cars.Count + 1 <= ParkingConf.MaxCount)
            {
                cars.Add(car);
            }
            else
            {
                Console.WriteLine("No Parking Places");
            }
        }

        public void AllTransport()
        {
            for(int i = 0; i < cars.Count; ++i)
            {
                Console.WriteLine($"Id: {i} | Car Name: {cars[i].CarName} | Car Type: {cars[i].Type} | Car Balance: {cars[i].Balance}");
            }
        }

        public void DeleteCar(int id)
        {
            try
            {
                cars.RemoveAt(id);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine("Out of range");                
            }
            
        }

        public void RefillCarBalance(int id, decimal money)
        {
            try
            {
                cars[id].Balance += money;
            }
            catch(ArgumentOutOfRangeException e)
            {
                Console.WriteLine("Out of range");
            }
        }

        public void CountOfPlaces()
        {
            Console.WriteLine($"Count of busy: {cars.Count}\nCount of not busy: {ParkingConf.MaxCount - cars.Count}");
        }

        public void AddLogPath(string path)
        {
            LogPath = path;
        }

        public void AllTransactions()
        {
            using (StreamReader r = File.OpenText(LogPath))
            {
                Logger.DumpLog(r);
            }
        }

        public void LogTransactions()
        {
            using (StreamWriter w = File.AppendText(LogPath))
            {
                foreach (var transaction in transactions)
                {
                    Logger.Log($"{transaction.Time} | {transaction.Type} | {transaction.TransactionCount}", w);
                }
            }
            transactions.Clear();
        }

        public void GetFee()
        {
            foreach(var car in cars)
            {
                decimal fee = ParkingConf.CarsPrice[car.Type];
                if(car.Balance - fee < 0)
                {
                    fee *= ParkingConf.FairCoeff;
                }
                car.Balance -= fee;
                Balance += fee;
                transactions.Add(new Transaction(fee, DateTime.Now, car.Type));
            }
        }

        public decimal MoneyForLastMinute()
        {
            decimal balance = 0;
            foreach(var transaction in transactions)
            {
                balance += transaction.TransactionCount;
            }
            return balance;
        }

        public void LastTransactions()
        {
            foreach (var transaction in transactions)
            {
                Console.WriteLine($"{transaction.Time} | {transaction.Type} | {transaction.TransactionCount}");
            }
        }

        public List<Car> GetCars()
        {
            return new List<Car>(cars);
        }

        public List<Transaction> GetTransactions()
        {
            return new List<Transaction>(transactions);
        }

        public int CountOfCars()
        {
            return cars.Count;
        }

        private static Parking parking = null;

        protected Parking()
        {
            cars = new List<Car>();
            transactions = new List<Transaction>();
            Balance = ParkingConf.StartBalance;
        }

        public static Parking Initilize()
        {
            if(parking == null)
            {
                parking = new Parking();
            }

            return parking;
        }

        public async Task RunTelepromter()
        {
            var startGettingFee = GetFeeAsync();
            var logTransactions = LogTransactionsAsync();

            await Task.WhenAny(startGettingFee, logTransactions);
        }

        public async Task GetFeeAsync()
        { 
            while (!ParkingConf.Done)
            {
                await Task.Delay(ParkingConf.TransactionPeriod * 1000);
                GetFee();
            }
        }

        public async Task LogTransactionsAsync()
        {
            while (!ParkingConf.Done)
            {
                await Task.Delay(ParkingConf.LogPeriod * 1000);
                LogTransactions();
            }
        }
    }
}
