﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parking
{
    public class Transaction
    {
        public DateTime Time { get; set; }
        public decimal TransactionCount { get; set; }
        public CarType Type { get; set; }

        public Transaction(decimal transactionCount, DateTime time, CarType type)
        {
            Type = type;
            Time = time;
            TransactionCount = transactionCount;
        }
    }
}
